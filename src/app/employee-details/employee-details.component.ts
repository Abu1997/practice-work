import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';


@Component({
  selector: 'employee-details',
  template: `
  <h2>Employee Details</h2>
  <ul *ngFor="let employee of employees">
  <li> {{employee.id}}. {{employee.name}}. {{employee.Age}}</li>
  </ul>
  `,
  styles: []
})
export class EmployeeDetailsComponent implements OnInit {

  public employees:any = [];

  constructor(private _employeeServives:EmployeeService) { }

  ngOnInit() {
    this._employeeServives.getEmployees()
         .subscribe (data => this.employees = data);
 }

}
